

$(document).ready(function () {


	// $(".icon-bar").click(function () {
	// 	$("header#header").toggleClass("active-menu");
	// });
	$(".toggle-search").click(function () {
		$(".wrap-search-header").toggleClass("active");
	});
	





	// menu 

	$(".wrap-icon-toggle").click(function () {
		$(this).toggleClass('active');
		$('body').toggleClass('overlay');
		$('nav#nav').toggleClass('active');
		$('header#header').toggleClass('active-menu');
		$('main#main').toggleClass('active-menu');
	});
	$(document).mouseup(function (e) {
		var container = $("nav#nav, header#header");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
				$('.wrap-icon-toggle').removeClass('active');
				$('body').removeClass('overlay');
				$('nav#nav').removeClass('active');
				$('header#header').removeClass('active-menu');
				$('main#main').removeClass('active-menu');
		}
	});
	function resize(){
		var win = $(window).width();
		if(win < 991){
			$('.title-category').click(function(){
				$(this).toggleClass('active');
				$('.wrap-list-category').slideToggle();
			});
			$('.icon-arrow-category').click(function(){
				$(this).siblings('.menu-child').slideToggle();
			})
		}

		if(win < 1025){
			$('.icon-arrow-menu').click(function(){
				$(this).toggleClass('active');
				$(this).siblings('.menu-child').slideToggle();
			})
		}
	}
	resize();
	$(window).on('resize', function(){
        resize();
    });

});
$('#slide-result').owlCarousel({
	loop: true,
	nav: true,
	dots: false,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 30,
	navText:[`<img src="./images/prev.png" alt="">`, `<img src="./images/next.png" alt="">`],
	responsive: {
		0: {
			items: 1,
		},
		768: {
			items: 2,
		},
	}
});



// fix header 
var prevScrollpos = window.pageYOffset;
window.onscroll = function () {
	var currentScrollPos = window.pageYOffset;
	if (prevScrollpos > currentScrollPos) {
		$("#header").css({
			"position": "sticky",
			"top": 0,
		});
		$("#header").addClass('active');
	} else {
		$("#header").css({
			"top": "-190px",
		});
		$("#header").removeClass('active');
	}
	prevScrollpos = currentScrollPos;
}

$(window).on('scroll', function () {
	if ($(window).scrollTop()) {
		$('#header:not(.header-project-detail-not-scroll)').addClass('active');
	} else {
		$('#header:not(.header-project-detail-not-scroll)').removeClass('active')
	};
});

function resizeImage() {
	let arrClass = [
		{ class: 'resize-result', number: (320 / 569) }, 
		{ class: 'size-image-video', number: (208 / 370) }, 
		{ class: 'resize-blog', number: (151 / 268) }, 
	];
	for (let i = 0; i < arrClass.length; i++) {
		if($("." + arrClass[i]['class']).length){
			let width = document.getElementsByClassName(arrClass[i]['class'])[0].offsetWidth;
			$("." + arrClass[i]['class']).css('height', width * arrClass[i]['number'] + 'px');
		}
	}
}

resizeImage();
new ResizeObserver(() => {
	resizeImage();
	
}).observe(document.body)

